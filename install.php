<?php
	/**
	 * Класс сценария установки демошаблона "demodizzy".
	 */
	class demodizzyInstallScenario extends siteInstallScenario implements iSiteInstallScenario{

		/**
		 * @inherit
		 */
		public function run() {
			$this->setCallOrderTemplate();
			$this->reIndexCategories();
			$this->setDomainForOrders();
		}

		/**
		 * Устанавливает шаблон писем для формы обратной связи "Заказать звонок"
		 * @throws coreException
		 */
		public function setCallOrderTemplate() {
			$objects = umiObjectsCollection::getInstance();
			$callOrderTemplate = $objects->getObjectByGUID('call-order-template');
			$types = umiObjectTypesCollection::getInstance();
			$callOrderFormId = $types->getTypeIdByGUID('call-order-form');

			if ($callOrderTemplate instanceof iUmiObject && is_numeric($callOrderFormId) && $callOrderFormId > 0) {
				$callOrderTemplate->setValue('form_id', $callOrderFormId);
				$callOrderTemplate->commit();
				$this->addLogMessage("Шаблон писем для формы с идентификатором ${callOrderFormId} установлен");
			}
		}

		/**
		 * Устанавливает домен для заказов в соответствующем поле
		 * @throws selectorException
		 */
		private function setDomainForOrders() {
			$orders = new selector('objects');
			$orders->types('hierarchy-type')->name('emarket', 'order');
			$result = $orders->result();

			$currentDomain = cmsController::getInstance()->getCurrentDomain();

			if (!$currentDomain instanceof domain) {
				return;
			}

			/** @var iUmiObject $order */
			foreach($result as $order) {
				$order->setValue('domain_id', $currentDomain->getId());
				$order->commit();
				$this->addLogMessage("Домен для заказа #{$order->getId()} успешно установлен");
			}
		}

		/**
		 * Запускает переиндексацию фильтров категорий
		 * @return bool
		 */
		private function reIndexCategories() {
			$this->addStartMessage();
			$indexedCategories = $this->getIndexedCategories();

			if (count($indexedCategories) == 0) {
				$this->addLogMessage('Не найдены категории, требующие переиндексации фильтров');
				$this->addEndMessage();
				return false;
			}

			$umiHierarchy = umiHierarchy::getInstance();

			/* @var iUmiHierarchyElement|umiEntinty $indexedCategory */
			foreach ($indexedCategories as $indexedCategory) {
				$this->reIndexCategory($indexedCategory);
				$categoryId = $indexedCategory->getId();
				$this->addLogMessage('Категория с id = ' . $categoryId . ' успешно переиндексирована');
				$umiHierarchy->unloadElement($categoryId);
			}

			$this->addEndMessage();
			return true;
		}

		/**
		 * Добавляет в лог сообщение о начале выполнения сценария
		 * @return void
		 */
		private function addStartMessage() {
			$this->addLogMessage('Начало переиндексации фильтров');
		}

		/**
		 * Возвращает категории разделов каталога, нуждающиеся в переиндексации
		 * @return array
		 */
		private function getIndexedCategories() {
			$categories = new selector('pages');
			$categories->types('hierarchy-type')->name('catalog', 'category');
			$categories->where('index_choose')->equals(true);
			return $categories->result();
		}

		/**
		 * Переиндексирует фильтры раздела каталога
		 * @param iUmiHierarchyElement $category объект раздела каталога
		 * @return true
		 */
		private function reIndexCategory(iUmiHierarchyElement $category) {
			$level = (int) $category->getValue('index_level');
			$parentId = $category->getId();
			$catalogObjectHierarchyTypeId = $this->getCatalogObjectHierarchyTypeId();

			$indexGenerator = new FilterIndexGenerator($catalogObjectHierarchyTypeId, 'pages');
			$indexGenerator->setHierarchyCondition($parentId, $level);
			$indexGenerator->run();

			$category->setValue('index_source', $parentId);
			$category->setValue('index_date', new umiDate());
			$category->setValue('index_state', 100);
			$category->commit();

			$this->markChildren($parentId, $level);
			return true;
		}

		/**
		 * Возвращает идентификатор иерархического типа данных объектов каталога,
		 * или false, если не удалось получить тип.
		 * @return bool|int
		 */
		private function getCatalogObjectHierarchyTypeId() {
			$umiHierarchyTypes = umiHierarchyTypesCollection::getInstance();
			$umiHierarchyType = $umiHierarchyTypes->getTypeByName('catalog', 'object');

			if (!$umiHierarchyType instanceof umiHierarchyType) {
				return false;
			}

			return $umiHierarchyType->getId();
		}

		/**
		 * Указывает у дочерних разделов каталога источник индекса фильтров
		 * @param int $parentId ид родительского раздела
		 * @param int $level уровень вложенности дочерних разделов
		 * @return bool
		 */
		private function markChildren($parentId, $level) {
			$childrenCategories = new selector('pages');
			$childrenCategories->types('hierarchy-type')->name('catalog', 'category');
			$childrenCategories->where('hierarchy')->page($parentId)->childs($level);
			$childrenCategories->order('id');
			$childrenCategories = $childrenCategories->result();

			if (count($childrenCategories) == 0) {
				false;
			}
			$umiHierarchy = umiHierarchy::getInstance();
			/* @var iUmiHierarchyElement|umiEntinty $childrenCategory */
			foreach ($childrenCategories as $childrenCategory) {
				$childrenCategory->setValue('index_source', $parentId);
				$childrenCategory->commit();
				$umiHierarchy->unloadElement($childrenCategory->getId());
			}
			return true;
		}

		/**
		 * Добавляет в лог сообщение о конце выполнения сценария
		 * @return void
		 */
		private function addEndMessage() {
			$this->addLogMessage('Конец переиндексации фильтров');
		}
	}
?>